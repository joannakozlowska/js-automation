const {Header, Footer, FooterScocial} = require('../components/common');

module.exports = class BasePage {
  constructor(url) {
    this.url = url;
    this.header = new Header();
    this.footer = new Footer();
    this.footerScocial = new FooterScocial();
  }

  open() {
    return browser.url(this.url);
  }
};
