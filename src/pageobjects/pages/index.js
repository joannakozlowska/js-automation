const AccountPage = require('./account.page')
const LoginPage = require('./login.page')
function page(name) {
    const items = { 
        loginpage: new LoginPage(),
        accountpage: new AccountPage()


    }; 
    return items[name.toLowerCase()];

}
module.exports = {
  AccountPage,
  LoginPage,
  page,
};

