const Page = require('./base.page');


module.exports = class AccountPage extends Page {
  get title() {
    return $('.maintext');
  }

  get editAccountDetails() {
    return $('ul.nav-dash li:nth-child(1) a');
  }

  get changePassword() {
    return $('ul.nav-dash li:nth-child(2) a');
  }

  get manageAddressBookFromTopTile() {
    return $('ul.nav-dash li:nth-child(3) a');
  }

  get wishListFromTopTile() {
    return $('ul.nav-dash li:nth-child(4) a');
  }

  get orderHistoryFromTopTile() {
    return $('ul.nav-dash li:nth-child(5) a');
  }

  get transactionHistoryFromTopTile() {
    return $('ul.nav-dash li:nth-child(6) a');
  }
  get downloadsFromTopTile() {
    return $('ul.nav-dash li:nth-child(7) a');
  }

  get notifications() {
    return $('ul.nav-dash li:nth-child(8) a');
  }

  get logOff() {
    return $('ul.nav-dash li:nth-child(9) a');
  }


  get manageAddressBook() {
    return $('//div[contains(., \'Manage Address Book\')]/ancestor::div[contains(@class, \'col-md-3\') and contains(@class, \'col-sm-6\')]');
  }

  get orderHistory() {
    return $('//div[contains(., \'Order history\')]/ancestor::div[contains(@class, \'col-md-3\') and contains(@class, \'col-sm-6\')]');
  }

  get downloads() {
    return $('//div[contains(., \'Downloads\')]/ancestor::div[contains(@class, \'col-md-3\') and contains(@class, \'col-sm-6\')]');
  }

  get transactionHistory() {
    return $('//div[contains(., \'Transaction history\')]/ancestor::div[contains(@class, \'col-md-3\') and contains(@class, \'col-sm-6\')]');
  }
};
