const BaseComponent = require('../common/base.component');

module.exports = class Login extends BaseComponent {
  constructor() {
    super('.maintext');
  }

  get btnSubmit() {
    return this.rootElement.$('button[title="Login"]');
  }
/**
   * @param name {'name' | 'password' }
   */
input(name) {
    const selectors = {
      name: '#loginFrm_loginname',
      password: '#loginFrm_password',
    };

    return this.rootEl.$(selectors[name.toLowerCase()]);
  }

//   async open() {
//     await super.open('index.php?rt=account/login');
//   }

//   async submit() {
//     await this.btnSubmit.click();
//   }
};
