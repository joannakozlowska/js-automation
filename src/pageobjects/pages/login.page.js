const Page = require('./base.page');
const {Login} = require('../components');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends BasePage {
  constructor() {
    super('index.php?rt=account/login');
    this.Login = new Login();
  }
}

module.exports = new LoginPage();
