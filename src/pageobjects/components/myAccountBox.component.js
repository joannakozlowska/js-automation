const BaseComponent = require('./common/base.component');

module.exports = class MyAccountBox extends BaseComponent {
  constructor() {
    super('.myaccountbox');
  }
  get accountDashboard() {
    return this.rootElement.$('.myaccountbox li:nth-child(1) a');
  }

  get wishList() {
    return this.rootElement.$('.myaccountbox li:nth-child(2) a');
  }

  get editAccountDetails() {
    return this.rootElement.$('.myaccountbox li:nth-child(3) a');
  }

  get changePassword() {
    return this.rootElement.$('.myaccountbox li:nth-child(4) a');
  }

  get manageAddressBook() {
    return this.rootElement.$('.myaccountbox li:nth-child(5) a');
  }

  get orderHistory() {
    return this.rootElement.$('.myaccountbox li:nth-child(6) a');
  }

  get transactionsHistory() {
    return this.rootElement.$('.myaccountbox li:nth-child(7) a');
  }

  get downloads() {
    return this.rootElement.$('.myaccountbox li:nth-child(8) a');
  }

  get notifications() {
    return this.rootElement.$('.myaccountbox li:nth-child(9) a');
  }

  get logOff() {
    return this.rootElement.$('.myaccountbox li:nth-child(10) a');
  }
};
