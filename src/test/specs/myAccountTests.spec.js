// const LoginPage = require('../../pageobjects/pages/login.page');
// const AccountPage = require('../../pageobjects/pages/account.page');

describe('Verify My Account page', () => {
  beforeEach(async () => {
        await browser.reloadSession();
        await browser.url('https://automationteststore.com'); 
    });

  it('Should contain all elements', async () => {
    await $('#customer_menu_top').click();
    await $('#loginFrm_loginname').setValue('JaneDoeUITester');
    await $('#loginFrm_password').setValue('Tester1234!');
    await $('button[title="Login"]').click();

    const accountDashboard = await $('.myaccountbox li:nth-child(1) a');
    const wishList = await $('.myaccountbox li:nth-child(2) a');
    const editAccountDetails = await $('.myaccountbox li:nth-child(3) a');
    const changePassword = await $('.myaccountbox li:nth-child(4) a');
    const manageAddressBook = await $('.myaccountbox li:nth-child(5) a');
    const orderHistory = await $('.myaccountbox li:nth-child(6) a');
    const transactionsHistory = await $('.myaccountbox li:nth-child(7) a');
    const downloads = await $('.myaccountbox li:nth-child(8) a');
    const notifications = await $('.myaccountbox li:nth-child(9) a');
    const logOff = await $('.myaccountbox li:nth-child(10) a');

    expect(await accountDashboard.isDisplayed()).toBe(true);
    expect(await wishList.isDisplayed()).toBe(true);
    expect(await editAccountDetails.isDisplayed()).toBe(true);
    expect(await changePassword.isDisplayed()).toBe(true);
    expect(await manageAddressBook.isDisplayed()).toBe(true);
    expect(await orderHistory.isDisplayed()).toBe(true);
    expect(await transactionsHistory.isDisplayed()).toBe(true);
    expect(await downloads.isDisplayed()).toBe(true);
    expect(await notifications.isDisplayed()).toBe(true);
    expect(await logOff.isDisplayed()).toBe(true);
  })
});
