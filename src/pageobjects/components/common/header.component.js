const BaseComponent = require('../common/base.component');

module.exports = class HeaderComponent extends BaseComponent {
  constructor() {
    super('//header');
  }
  get loginButton() {
    return this.rootElement.$('//*[contains(text(), \'Login or register\')]');
  }
};
