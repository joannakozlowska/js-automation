const BaseComponent = require('./base.component');

module.exports = class Footer extends BaseComponent {
  constructor() {
    super('.footerlinks');
  }

  get aboutUs() {
    return this.rootElement.$('ul.info_links_footer li:nth-child(1) a');
  }

  get privacyPolicy() {
    return this.rootElement.$('ul.info_links_footer li:nth-child(2) a');
  }

  get returnPolicy() {
    return this.rootElement.$('ul.info_links_footer li:nth-child(3) a');
  }

  get shipping() {
    return this.rootElement.$('ul.info_links_footer li:nth-child(4) a');
  }

  get contactUs() {
    return transactionHistory.rootElement.$('ul.info_links_footer li:nth-child(5) a');
  }

  get siteMap() {
    return this.rootElement.$('ul.info_links_footer li:nth-child(6) a');
  }

  get login() {
    return this.rootElement.$('ul.info_links_footer li:nth-child(7) a');
  }

  get facebookLink() {
    return this.rootElement.$('.footerlinks .facebook');
  }

  get twitterLink() {
    return this.rootElement.$('.footerlinks .twitter');
  }

  get linkedinLink() {
    return this.rootElement.$('.footerlinks .linkedin');
  }
};
