// const page = require('../../pageobjects');

describe('Login as registered user', () => {

    beforeEach(async () => {
        await browser.reloadSession();
        await browser.url('https://automationteststore.com');     
    });

    it('Should open the page', async () => {
        await $('#customer_menu_top').click();
        await expect(browser).toHaveTitle('Account Login');
    });

    it('should login with valid credentials', async () => {
        await $('#customer_menu_top').click();
        await $('#loginFrm_loginname').setValue('JaneDoeUITester');
        await $('#loginFrm_password').setValue('Tester1234!');
        await $('button[title="Login"]').click();
        const pageTitle = await $('.maintext');
        await expect(pageTitle).toBeExisting();
        await expect(pageTitle).toHaveText('My Account', {ignoreCase: true});
    });

    it('Should log off user', async () => {
        await $('#customer_menu_top').click();
        await $('#loginFrm_loginname').setValue('JaneDoeUITester');
        await $('#loginFrm_password').setValue('Tester1234!');
        await $('button[title="Login"]').click();
        await $('.myaccountbox li:nth-child(10) a').click();
        const pageTitle = await $('.maintext');
        await expect(pageTitle).toBeExisting();
        await expect(pageTitle).toHaveText('ACCOUNT LOGOUT', {ignoreCase: true});
    });
})


