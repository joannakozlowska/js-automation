describe('Test suite', () => {
  it('Check page title', async () => {
    await browser.url('https://automationteststore.com/');

    const pageTitle = await browser.getTitle();
    expect(pageTitle).toEqual('A place to practice your automation skills!');
  });
});
