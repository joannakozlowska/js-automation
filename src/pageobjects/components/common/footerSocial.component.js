const BaseComponent = require('../common/base.component');

module.exports = class FooterSocal extends BaseComponent {
  constructor() {
    super('.footersocial');
  }
  get aboutUs() {
    return this.rootElement.$('#block_frame_html_block_1931');
  }

  get contactUS() {
    return this.rootElement.$('#block_frame_html_block_1929');
  }

  get testimonials() {
    return this.rootElement.$('#testimonialsidebar');
  }

  get newsletterSignup() {
    return this.rootElement.$('#newslettersignup');
  }
};
