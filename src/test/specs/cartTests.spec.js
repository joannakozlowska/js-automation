// const { page } = require("../../pageobjects/pages")

describe('Add item to the cart', async () => {
    beforeEach(async () => {
        await browser.reloadSession();
        await browser.url('https://automationteststore.com/');
            });

        
    it('should add item to the cart', async () => {
        const detailButton = await $('.thumbnail');
        await detailButton.waitForClickable();
        await detailButton.click();
        await $('.cart').click();
        const quantityElement = await $('//div[@class=\'input-group input-group-sm\']//input[@id=\'cart_quantity50\']');
        const quantity = await quantityElement.getAttribute('value');
        expect(quantity).toEqual('1');
      });

    it('Remove previously added item from cart', async () => {
    const detailButton = await $('.thumbnail');
    await detailButton.waitForClickable();
    await detailButton.click();
    await $('.cart').click();
    await $('td:nth-child(7) > a').click();
    const emptyCartMessage = await $('.contentpanel').getText();
    expect(emptyCartMessage).toHaveText('Your shopping cart is empty!', {ignoreCase: true});
    });

    it('Clicking on Checkout button should redirect to login page', async () => {
        const detailButton = await $('.thumbnail');
        await detailButton.waitForClickable();
        await detailButton.click();
        await $('.cart').click();
        await $('#cart_checkout1').click();
        const pageTitle = await $('.maintext');
        expect(pageTitle).toHaveText('Account Login', {ignoreCase: true});
    });

    it('Clicking on Checkout button for logged in user should redirect to checkout confirmation', async () => {
        await $('#customer_menu_top').click();
        await $('#loginFrm_loginname').setValue('JaneDoeUITester');
        await $('#loginFrm_password').setValue('Tester1234!');
        await $('button[title="Login"]').click();
        await $('.logo').click();
        const detailButton = await $('.thumbnail');
        await detailButton.waitForClickable();
        await detailButton.click();
        await $('.cart').click();
        await $('#cart_checkout1').click();
        const pageTitle = await $('.maintext');
        expect(pageTitle).toHaveText('checkout confirmation', {ignoreCase: true});

    });

    it('Complete an order as logged in user', async () => {
        await $('#customer_menu_top').click();
        await $('#loginFrm_loginname').setValue('JaneDoeUITester');
        await $('#loginFrm_password').setValue('Tester1234!');
        await $('button[title="Login"]').click();
        await $('.logo').click();
        const detailButton = await $('.thumbnail');
        await detailButton.waitForClickable();
        await detailButton.click();
        await $('.cart').click();
        await $('#cart_checkout1').click();
        await $('#checkout_btn').click();
        const pageTitle = await $('.maintext');
        expect(pageTitle).toHaveText('YOUR ORDER HAS BEEN PROCESSED!', {ignoreCase: true});

    });
})

